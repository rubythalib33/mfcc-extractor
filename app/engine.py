import numpy as np
import scipy.fftpack as fft
from scipy.signal.windows import hamming


def framing(audio:np.ndarray, frame_length:int=25, frame_shift:int=10, sr:int=16000) -> np.ndarray:
    frame_length_hz = frame_length*sr//1000
    frame_shift_hz = frame_shift*sr//1000
    print(frame_shift_hz)
    print(frame_length_hz)
    result = []
    start = 0

    while True:
        temp = audio[start:start+frame_length_hz]
        if len(temp) == frame_length_hz:
            result.append(temp)
        start += frame_shift_hz

        if start >= len(audio):
            break

    return np.array(result, dtype=np.float32)


def windowing(framed:np.ndarray, frame_length:int, sr:int=16000)->np.ndarray:
    window = hamming(frame_length*sr//1000)
    return framed*window


def discrete_fourier_transforms(windowed:np.ndarray, frame_length:int, sr:int=16000)->np.ndarray:
    windowed_transposed = np.transpose(windowed)
    audio_fft = np.empty((1+(frame_length*sr//1000)//2, windowed_transposed.shape[1]), dtype=np.complex64, order='F')

    for n in range(audio_fft.shape[1]):
        audio_fft[:, n] = fft.fft(windowed_transposed[:, n], axis=0)[:audio_fft.shape[0]]
    
    return np.transpose(audio_fft)


def freq_to_mel(freq:int)->float:
    return 2595.0 * np.log10(1.0 + freq / 700.0)


def mel_to_freq(mels:int)->float:
    return 700.0 * (10.0**(mels / 2595.0) - 1.0)


def get_filter_points(fmin:int, fmax:int, num_mel_bins:int, frame_length:int, sr:int=16000)->np.ndarray:
    fmin_mel = freq_to_mel(fmin)
    fmax_mel = freq_to_mel(fmax)
    
    print("MEL min: {0}".format(fmin_mel))
    print("MEL max: {0}".format(fmax_mel))
    
    mels = np.linspace(fmin_mel, fmax_mel, num=num_mel_bins+2)
    freqs = mel_to_freq(mels)
    
    return np.floor((frame_length*sr//1000 + 1) / sr * freqs).astype(int), freqs


def get_filters(filter_points:np.ndarray, frame_length:int, sr:int=16000)->np.ndarray:
    filters = np.zeros((len(filter_points)-2,int((frame_length*sr//1000)/2+1)))
    
    for n in range(len(filter_points)-2):
        filters[n, filter_points[n] : filter_points[n + 1]] = np.linspace(0, 1, filter_points[n + 1] - filter_points[n])
        filters[n, filter_points[n + 1] : filter_points[n + 2]] = np.linspace(1, 0, filter_points[n + 2] - filter_points[n + 1])
    
    return filters


def mel_frequency_filter_bank(audio_power:np.ndarray, num_mel_bins:int, frame_length:int, sr:int=16000)->np.ndarray:
    freq_min = 0
    freq_high = sr / 2
    filter_points, mel_freqs = get_filter_points(fmin=freq_min, fmax=freq_high, num_mel_bins=num_mel_bins, frame_length=frame_length, sr=sr)
    filters = get_filters(filter_points, frame_length=frame_length, sr=sr)
    enorm = 2.0 / (mel_freqs[2:num_mel_bins+2] - mel_freqs[:num_mel_bins])
    filters *= enorm[:, np.newaxis]
    audio_filtered = np.dot(filters, np.transpose(audio_power))

    return audio_filtered


def discrete_cosine_transforms(dct_filter_num:int, filter_len:int)->np.ndarray:
    basis = np.empty((dct_filter_num,filter_len))
    basis[0, :] = 1.0 / np.sqrt(filter_len)
    
    samples = np.arange(1, 2 * filter_len, 2) * np.pi / (2.0 * filter_len)

    for i in range(1, dct_filter_num):
        basis[i, :] = np.cos(i * samples) * np.sqrt(2.0 / filter_len)
        
    return basis


def mfcc_extractor(x:np.ndarray, frame_length:int=25, frame_shift:int=10, num_mel_bins:int=10, dct_filter_num:int=13, sr:int = 16000)->np.ndarray:
    framed = framing(x, frame_length=frame_length, frame_shift=frame_shift, sr=sr)
    
    windowed = windowing(framed, frame_length=frame_length, sr=sr)
    
    audio_fft = discrete_fourier_transforms(windowed=windowed, frame_length=frame_length, sr=sr)
    
    audio_power = np.square(np.abs(audio_fft))
    
    audio_filtered = mel_frequency_filter_bank(audio_power=audio_power, num_mel_bins=num_mel_bins, frame_length=frame_length, sr=sr)
    
    audio_log = 10.0 * np.log10(audio_filtered)
    
    dct_filters = discrete_cosine_transforms(dct_filter_num=dct_filter_num, filter_len=num_mel_bins)
    cepstral_coeffisien = np.dot(dct_filters, audio_log)

    return np.transpose(cepstral_coeffisien)


if __name__ == '__main__':
    import librosa

    input_path = "../sample_input/audio_sample.wav"
    audio, sample_rate = librosa.load(input_path, sr=16000)
    print(type(audio))
    print(len(audio)/sample_rate)
    print(sample_rate)

    result = mfcc_extractor(audio)

    print(result.shape)
    