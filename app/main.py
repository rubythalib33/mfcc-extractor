from fastapi import FastAPI, UploadFile, File
from .engine import mfcc_extractor
from io import BytesIO

import librosa

app = FastAPI()


@app.get("/")
async def read_root():
    return {"Welcome": "to MFCC Extractor Engine"}


@app.post("/mfcc-extractor")
async def mfcc_extractor_api(audio: UploadFile=File(...), frame_length:int=25,frame_shift:int=10, num_mel_bins:int=10, dct_filter_num:int=13, sample_rate:int=16000):
    audio = await audio.read()
    audio, sample_rate = librosa.load(BytesIO(audio), sr=sample_rate)
    

    cepstral_coeffisien = mfcc_extractor(x=audio, frame_length=frame_length, frame_shift=frame_shift, num_mel_bins=num_mel_bins, dct_filter_num=dct_filter_num,sr=sample_rate)

    cepstral_coeffisien = cepstral_coeffisien.tolist()

    return {"cepstral_coeffisien":cepstral_coeffisien}
