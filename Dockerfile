FROM python:3.9

ADD . .

RUN apt update && apt install -y libsndfile-dev

RUN pip install -r requirements.txt

CMD ["uvicorn","app.main:app","--reload", "--host", "0.0.0.0", "--port", "5555"]