# MFCC Extractor

# Requirements
```bash
python 3.9
fastapi 0.79.0
uvicorn[standard] 0.18.2
numpy 1.21.6
scipy 1.7.3
librosa 0.9.2
```

# How to Setup
```bash
git clone https://gitlab.com/rubythalib33/mfcc-extractor.git
cd mfcc-extractor
docker build -t mfcc:latest .
```

# How to Run
```bash
sh start.sh
```
for interactive swegger UI, after running the docker go to http://0.0.0.0:5555/docs

# API documentations
POST /mfcc-extractor

params:
1. audio, upload file (already tested on .wav)
2. frame_length, (int) frame length input for input frame_length in framing audio function (in ms)
3. frame_shift, (int) frame shift input for input frame_shift in framing audio function (in ms)
4. num_mel_bins, (int) the number of mel filter
5. dct_filter_nums, (int) the number of dct filters
6. sample_rate, (int) the number of sample rate that you want to resample the audio input

outputs:
matrix with size Nx{dct_filter_nums} the default is Nx13